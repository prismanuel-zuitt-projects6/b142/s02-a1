package com.manuel.b142.s02.s02app.models;

//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.Table;
import javax.persistence.*;

@Entity
@Table(name="posts")
public class Post {
    // Properties (columns)
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String title;
    @Column
    private String content;
    // Constructors
    public Post(){}

    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }
    // Data: title, content

    // Getters & Setters
    public String getTitle() {
        return title;
    }
    public String getContent() {
        return content;
    }

    public void setTitle(String newTitle) {
        this.title = newTitle;
    }
    public void setContent(String newContent) {
        this.content = newContent;
    }

    // Methods
}
