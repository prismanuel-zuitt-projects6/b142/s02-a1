package com.manuel.b142.s02.s02app.repositories;

import com.manuel.b142.s02.s02app.models.Post;
import org.springframework.data.repository.CrudRepository;

@Repository
public interface PostRepository extends CrudRepository<Post, Object> {
}
